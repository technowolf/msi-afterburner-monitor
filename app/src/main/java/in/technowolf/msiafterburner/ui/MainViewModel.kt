/*
 * Copyright (c) 2020 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package `in`.technowolf.msiafterburner.ui

import `in`.technowolf.msiafterburner.data.AfterburnerRS
import `in`.technowolf.msiafterburner.data.AfterburnerRepo
import `in`.technowolf.msiafterburner.utils.AfterburnerException
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import arrow.core.Either
import com.google.gson.Gson
import fr.arnaudguyon.xmltojsonlib.XmlToJson
import kotlinx.coroutines.launch

class MainViewModel(private val afterburnerRepo: AfterburnerRepo) : ViewModel() {

    private val gson = Gson()

    private var afterburnerRS: AfterburnerRS? = null

    private var _errorObserver = MutableLiveData<AfterburnerException>()
    val errorObserver: LiveData<AfterburnerException> get() = _errorObserver

    private var _afterburnerData = MutableLiveData<AfterburnerRS>()
    val afterburnerData: LiveData<AfterburnerRS> get() = _afterburnerData

    fun getAfterburnerData() {
        viewModelScope.launch {
            val response = afterburnerRepo.getMonitoringConfig()
            when (response) {
                is Either.Left -> {
                    _errorObserver.postValue(response.a)
                }
                is Either.Right -> {
                    val a = XmlToJson.Builder(response.b).build()
                    val b = a.toString()
                    afterburnerRS = gson.fromJson(b, AfterburnerRS::class.java)
                    _afterburnerData.postValue(afterburnerRS)
                }
            }
        }
    }

}