/*
 * Copyright (c) 2020 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package `in`.technowolf.msiafterburner.ui

import `in`.technowolf.msiafterburner.R
import `in`.technowolf.msiafterburner.utils.AfterburnerException
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {

    private val mainViewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel.getAfterburnerData()

        mainViewModel.afterburnerData.observe(this, {
            Log.i("DATA", it.toString())
        })

        mainViewModel.errorObserver.observe(this, { handleExceptions(it) })
    }

    private fun handleExceptions(exception: AfterburnerException) {
        when (exception) {
            AfterburnerException.AfterburnerNotRunning -> {
                Toast.makeText(this, "Afterburner Not running", Toast.LENGTH_SHORT).show()
            }
            is AfterburnerException.RemoteServerNotRunning -> {
                Toast.makeText(this, "Remote Server Not running", Toast.LENGTH_SHORT).show()
            }
            is AfterburnerException.GenericException -> {
                Toast.makeText(this, exception.errorMessage, Toast.LENGTH_SHORT).show()
            }
        }
    }
}