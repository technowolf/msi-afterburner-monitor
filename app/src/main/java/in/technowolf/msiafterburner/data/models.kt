/*
 * Copyright (c) 2020 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package `in`.technowolf.msiafterburner.data

import androidx.annotation.Keep

import com.google.gson.annotations.SerializedName


@Keep
data class AfterburnerRS(
    @SerializedName("HardwareMonitor")
    val monitor: Monitor
)

@Keep
data class Monitor(
    @SerializedName("HardwareMonitorEntries")
    val monitorEntries: MonitorEntries,
    @SerializedName("HardwareMonitorGpuEntries")
    val gpuEntries: GpuEntries,
    @SerializedName("HardwareMonitorHeader")
    val monitorMeta: MonitorMeta
)

@Keep
data class MonitorEntries(
    @SerializedName("HardwareMonitorEntry")
    val monitorEntry: List<MonitorEntry>
)

@Keep
data class GpuEntries(
    @SerializedName("HardwareMonitorGpuEntry")
    val gpuMeta: GpuMeta
)

@Keep
data class MonitorMeta(
    @SerializedName("entryCount")
    val entryCount: String,
    @SerializedName("entrySize")
    val entrySize: String,
    @SerializedName("gpuEntryCount")
    val gpuEntryCount: String,
    @SerializedName("gpuEntrySize")
    val gpuEntrySize: String,
    @SerializedName("headerSize")
    val headerSize: String,
    @SerializedName("signature")
    val signature: String,
    @SerializedName("time")
    val time: String,
    @SerializedName("version")
    val version: String
)

@Keep
data class MonitorEntry(
    @SerializedName("data")
    val value: String,
    @SerializedName("flags")
    val flags: String,
    @SerializedName("gpu")
    val gpu: String,
    @SerializedName("localizedSrcName")
    val localizedName: String,
    @SerializedName("localizedSrcUnits")
    val localizedUnits: String,
    @SerializedName("maxLimit")
    val maxLimit: String,
    @SerializedName("minLimit")
    val minLimit: String,
    @SerializedName("srcId")
    val srcId: String,
    @SerializedName("srcName")
    val entryName: String,
    @SerializedName("srcUnits")
    val dataUnit: String
)

@Keep
data class GpuMeta(
    @SerializedName("BIOS")
    val bios: String,
    @SerializedName("device")
    val gpuDevice: String,
    @SerializedName("driver")
    val driverVersion: String,
    @SerializedName("family")
    val family: String,
    @SerializedName("gpuId")
    val gpuId: String,
    @SerializedName("memAmount")
    val memAmount: String
)